package br.com.itau.pedidos.clients;

import org.springframework.stereotype.Component;

import br.com.itau.pedidos.models.Pagamento;

@Component
public class PagamentoClientFallback implements PagamentoClient {

	@Override
	public Pagamento verifica(Pagamento pagamento) {
		return null;
	}

}
