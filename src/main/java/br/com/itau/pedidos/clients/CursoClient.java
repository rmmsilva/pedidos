package br.com.itau.pedidos.clients;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import br.com.itau.pedidos.models.Curso;

@FeignClient("catalogo")
public interface CursoClient {

	@GetMapping
	List<Curso> listar();
}
