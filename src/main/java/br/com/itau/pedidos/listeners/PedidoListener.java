package br.com.itau.pedidos.listeners;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import br.com.itau.pedidos.models.Pedido;
import br.com.itau.pedidos.services.PedidoService;

@Component
public class PedidoListener {

	@Autowired
	PedidoService pedidoService;

	@KafkaListener(topics = "pedidos-analisados", groupId = "pedidos", properties = "auto.offset.reset=earliest")
	public void efetuarPedido(Pedido pedido) {
		pedido.setTimestamp(LocalDateTime.now());
		pedidoService.salvar(pedido);
	}

}
