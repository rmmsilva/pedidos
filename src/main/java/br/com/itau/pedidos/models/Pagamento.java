package br.com.itau.pedidos.models;

public class Pagamento {

	private double valor;

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

}
