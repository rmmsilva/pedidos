package br.com.itau.pedidos.models;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Pedido {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String nomeCliente;

	private String nomeCurso;

	private double precoCurso;

	private LocalDateTime timestamp;

	private LocalDateTime timestampAtualizacao;

	private String status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public String getNomeCurso() {
		return nomeCurso;
	}

	public void setNomeCurso(String nomeCurso) {
		this.nomeCurso = nomeCurso;
	}

	public double getPrecoCurso() {
		return precoCurso;
	}

	public void setPrecoCurso(double precoCurso) {
		this.precoCurso = precoCurso;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	public LocalDateTime getTimestampAtualizacao() {
		return timestampAtualizacao;
	}

	public void setTimestampAtualizacao(LocalDateTime timestampAtualizacao) {
		this.timestampAtualizacao = timestampAtualizacao;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
