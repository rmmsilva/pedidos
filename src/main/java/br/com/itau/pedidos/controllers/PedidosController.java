package br.com.itau.pedidos.controllers;

import static org.springframework.http.HttpStatus.NO_CONTENT;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.pedidos.models.Curso;
import br.com.itau.pedidos.models.Pedido;
import br.com.itau.pedidos.services.CursoService;
import br.com.itau.pedidos.services.PagamentoService;
import br.com.itau.pedidos.services.PedidoService;

@RestController
public class PedidosController {

	@Autowired
	CursoService cursoService;

	@Autowired
	PagamentoService pagamentoService;

	@Autowired
	PedidoService pedidoService;

	@PostMapping
	public ResponseEntity<Pedido> criarPedido(@RequestBody Pedido pedido) {

		Optional<Curso> curso = cursoService.busca(pedido.getNomeCurso());
		if (!curso.isPresent()) {
			return ResponseEntity
					.status(NO_CONTENT)
					.build();
		}

		pedido.setStatus("Pagamento pendente");
		pedido.setTimestamp(LocalDateTime.now());
		pedido.setTimestampAtualizacao(LocalDateTime.now());

		pedidoService.salvar(pedido);
		pagamentoService.verificaPagamento(pedido);

		return ResponseEntity.ok(pedido);
	}

	@GetMapping
	public ResponseEntity<List<Pedido>> buscarTodos() {
		return ResponseEntity.ok(pedidoService.buscarTodos());
	}

	@GetMapping("/{id}")
	public ResponseEntity<Pedido> buscar(@PathVariable int id) {
		return pedidoService.buscar(id)
				.map(ResponseEntity::ok)
				.orElse(ResponseEntity.notFound().build());
	}
}
