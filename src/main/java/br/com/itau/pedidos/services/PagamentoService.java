package br.com.itau.pedidos.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import br.com.itau.pedidos.clients.PagamentoClient;
import br.com.itau.pedidos.models.Curso;
import br.com.itau.pedidos.models.Pagamento;
import br.com.itau.pedidos.models.Pedido;

@Service
public class PagamentoService {

	@Autowired
	PagamentoClient pagamentoClient;

	@Autowired
	KafkaTemplate<String, Object> template;

	public boolean verificaStatus(Curso curso) {
		Pagamento pagamento = new Pagamento();
		pagamento.setValor(curso.getPreco());

		return pagamentoClient.verifica(pagamento) != null;
	}

	public void verificaPagamento(Pedido pedido) {
		template.send("analise-pedidos", pedido);
	}
}
