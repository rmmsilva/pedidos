package br.com.itau.pedidos.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.pedidos.clients.CursoClient;
import br.com.itau.pedidos.models.Curso;

@Service
public class CursoService {

	@Autowired
	CursoClient cursoClient;

	public Optional<Curso> busca(String nomeCurso) {
		return cursoClient.listar()
				.stream()
				.filter(c -> c.getNome().equals(nomeCurso))
				.findFirst();
	}
}
